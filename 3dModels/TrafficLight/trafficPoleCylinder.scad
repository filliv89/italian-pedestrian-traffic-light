$fn = 100;

innerCylDiam = 8;
middleCylDiam =12;
externalCylDiam = 20;
 
 
module bigColumn(){

    heigthCyl = 140;
	
    
	difference(){
	//Big column	    
		translate([45,65,0])
		difference(){
			cylinder(h = heigthCyl, d = externalCylDiam);
			cylinder(h = heigthCyl, d = innerCylDiam );
		};
	translate([53,65,50])
	sphere(9);		
	}
	
	
	
	
}
module holeForAbove(){
	//hole for previous Cylinder
	heigthJointCyl = 37;
	translate([45,65,140])
	difference(){
        cylinder(h = heigthJointCyl, d = externalCylDiam);
        cylinder(h = heigthJointCyl, d = middleCylDiam );
    };	
}

module baseWithColumn(){
	
	cube([ 163,125.5,5]);
	bigColumn();
	holeForAbove();
}

module main(){

  baseWithColumn();   
}

main();