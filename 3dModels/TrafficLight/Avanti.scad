panel_width = 76;
panel_height = 15;
panel_thickness = 4;
text_thickness = 5;  // Ridotto a 1 mm
text_message = "AVANTI";

module createPanel() {
    cube([panel_width, panel_height, panel_thickness]);
}

module createText() {
    linear_extrude(height = text_thickness)
        text(text_message, size = 15, valign="center");
}

module textOnPanel() {
    difference() {
        createPanel();
        translate([panel_thickness/2, 7, 0])
            createText();
    }
}

textOnPanel();
