 
caseHeight = 25;

bedWidth = 48;
bedDepth = 21.65;
bedHeight = 15;

globalMargin = 5;

caseDepth = bedDepth*3;
witdhHole = 15;
depthHole = 25;

module azNanoSocket(){

	difference(){
		//Main box
		color("white")
		cube([bedWidth, bedDepth, bedHeight]); 
	
		//main cavity
		color("blue")	
		translate([1,2,5])
		cube([45, 17.65, 15]);	

		//under cavity
		color("red")	
		translate([3,2,4])
		cube([40, 17.65, 15]);	

		//sideAperture
		color("green")
		translate([3,0,4])
		cube([30, bedDepth, 15]);	
		
		//usbAperture
		color("yellow")
		translate([0,5.925,globalMargin])
		cube([9.80, 9.9,18]);

	}
}
module lowerCase(){

	difference() {
		//Main box
		cube([bedWidth, caseDepth, bedHeight*2]); 
		
		//main cavity
		color("green")
		translate([2,2,globalMargin]) 
		cube([bedWidth-4, caseDepth -4, bedHeight*2]);	

		//hole for connector
		color("yellow")
		translate([0,8,globalMargin])
		cube([9.80, 9.80,25]);

		//south hole for insertion
		holeLockSN("S");

		//North hole for insertion
		holeLockSN("N");

		//leftHole for cable
		holeLockLR("L");

		//leftHole for cable
		holeLockLR("R");
	}	
}

module holeLockLR(align = "L"){


    translateValueX = (align == "L") ? 0 : bedWidth -2;
	
	color("blue")
	translate([ translateValueX ,(caseDepth/2)  ,7 ])
	cube([2,depthHole,2]);

	color("red")
	translate([ translateValueX ,(caseDepth/2)  ,21 ])
	cube([2,depthHole,2]);
	
}

module holeLockSN(align = "S"){


    translateValueY = (align == "S") ? 0 : caseDepth -2;
	echo(align, translateValueY);
	color("blue")
	translate([ (bedWidth /2) - (witdhHole / 2) ,translateValueY ,bedHeight*2 -5 ])
	cube([witdhHole,2,2]);
	
}

module upperPlate(){

	//upperPlate
	color("blue")
	cube(size=[bedWidth, caseDepth, 3]);
	//innerBlockSpace 
	color("orange")
	translate([3,3,3])
	cube(size=[bedWidth-6, caseDepth-6, 3]);

	//southStepLock  
	translate([ (bedWidth-5)/2 - (witdhHole / 2) +2 ,3,6])
	stepLock();

	//norhtStepLock  
	translate([ (bedWidth-4)/2 - (witdhHole / 2) +2 , (caseDepth-4 ) ,6])
	rotate(a=180, v=[0, 0, 1])
	translate([-15,-1,0])
	stepLock();


	//coverHole for connector 
		color("yellow")
		translate([bedWidth-3,8,3])
		cube([3, 9.80,12]);

	

} 

module stepLock(){
	color("green")
	translate([0,0,2])
	rotate([0,90,0])
	cylinder(h=witdhHole, r=1.5);
	color("red")
	cube(size=[witdhHole, 2,3.2], center=false);
}

module main(){

	lowerCase();
	translate([0,2,5])
	azNanoSocket();
	
	//upper Case with hole for button
	translate([70,0,0])
	difference(){
		upperPlate();
		$fn = 100;
		color("red")
		
		//Optionals
		//big hole for button
		translate([15,45,0])
		cylinder(h=8,r=6);
		
		//hole for switch
		translate([35,45,0])
		cylinder(h=2,d=7);
	translate([35,45,2])
		cylinder(h=6,d=14);
	}
	

	

}
 
main();