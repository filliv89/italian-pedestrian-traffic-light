const int digitPedestrianRedPin = 2;
const int digitPedestrianGreenPin = 4;
const int digitPedestrianWhitePin = 3;

const int digitVeicolRedPin = 10;
const int digitVeicolYellowPin = 9;
const int digitVeicolGreenPin = 8;

const int digitCallButton = 6;
const int digitModeSwitchButton = 7;

bool trafficLightActive;

unsigned long int lightsTimer = 0;
unsigned long int blinkingGreenTimer = 0;

unsigned long int blinkingTime = 600UL;

unsigned long int nowTime;

unsigned long int maxTimeBlinkingGreen = 7000UL;  //blinking time of green ( Yellow pedestrian )

enum VeicolePhases {
  red,
  green,
  greenYellow,
  redP,
  greenP,
  blinkingGreenP
};
VeicolePhases phaseV, nextPhaseV;

enum blinkingPhases {
  high,
  none
};
blinkingPhases blinking, nextBlinking;


int buttonState = 0;  // variable for reading the pushbutton status

void downAll(bool handleWhitePedestrian = false) {


  digitalWrite(digitPedestrianRedPin, LOW);
  if (handleWhitePedestrian) {
    digitalWrite(digitPedestrianWhitePin, LOW);
  }
  digitalWrite(digitPedestrianGreenPin, LOW);

  digitalWrite(digitVeicolRedPin, LOW);
  digitalWrite(digitVeicolYellowPin, LOW);
  digitalWrite(digitVeicolGreenPin, LOW);
}

void setup() {

  Serial.begin(9600);
  lightsTimer = 0;
  //Setup Enum
  blinking = high;
  nextBlinking = none;

  phaseV = greenYellow;
  nextPhaseV = greenYellow;


  // Register pinMode
  pinMode(digitCallButton, INPUT_PULLUP);
  pinMode(digitModeSwitchButton,INPUT_PULLUP);

  pinMode(digitPedestrianRedPin, OUTPUT);
  pinMode(digitPedestrianGreenPin, OUTPUT);
  pinMode(digitPedestrianWhitePin, OUTPUT);


  pinMode(digitVeicolRedPin, OUTPUT);
  pinMode(digitVeicolYellowPin, OUTPUT);
  pinMode(digitVeicolGreenPin, OUTPUT);

  downAll();


  //test singolary
  digitalWrite(digitPedestrianRedPin, HIGH);
  digitalWrite(digitVeicolRedPin, HIGH);
  delay(1000);
  digitalWrite(digitPedestrianWhitePin, HIGH);
  digitalWrite(digitVeicolYellowPin, HIGH);
  delay(1000);
  digitalWrite(digitPedestrianGreenPin, HIGH);
  digitalWrite(digitVeicolGreenPin, HIGH);
  delay(1000);

  downAll();
  //isTrafficLightActive();
  trafficLightActive = digitalRead(digitModeSwitchButton);

  //TEST BUTTON
  pinMode(LED_BUILTIN, OUTPUT);


  //this one must be always HIGH
}

bool isTrafficLightActive() {

  trafficLightActive = digitalRead(digitModeSwitchButton);
 
  return trafficLightActive;
}

void loop() {
  // put your main code here, to run repeatedly:


  buttonState = digitalRead(digitCallButton);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on:
    digitalWrite(LED_BUILTIN, HIGH);
  } else {
    // turn LED off:
    digitalWrite(LED_BUILTIN, LOW);
  }

  if (trafficLightActive) {

    normalTrafficPhase();
  } else {

    disabledBlinkingPhase();
  }
}

void normalTrafficPhase() {


  if(phaseV == green && (digitalRead(digitCallButton)) == LOW ){

     lightsTimer = 0;
     nextPhaseV = greenYellow;
  }

  Serial.println(digitalRead(digitCallButton));


  if (millis() >= lightsTimer) {


    phaseV = nextPhaseV;
    switch (phaseV) {
      case red:

        digitalWrite(digitVeicolYellowPin, LOW);
        digitalWrite(digitVeicolGreenPin, LOW);
        digitalWrite(digitVeicolRedPin, HIGH);

        digitalWrite(digitPedestrianGreenPin, LOW);
        digitalWrite(digitPedestrianRedPin, HIGH);


        //set greeen Pedestrian
        nextPhaseV = greenP;
        lightsTimer = millis() + 2000UL;  //2 Sec
        break;

      case greenP:
        digitalWrite(digitVeicolYellowPin, LOW);
        digitalWrite(digitVeicolGreenPin, LOW);
        digitalWrite(digitVeicolRedPin, HIGH);

        digitalWrite(digitPedestrianGreenPin, HIGH);
        digitalWrite(digitPedestrianRedPin, LOW);


        //set greeen Pedestrian
        nextPhaseV = blinkingGreenP;
        lightsTimer = millis() + 15000UL;  //15 Sec

        break;

      case blinkingGreenP:

        digitalWrite(digitVeicolYellowPin, LOW);
        digitalWrite(digitVeicolGreenPin, LOW);
        digitalWrite(digitVeicolRedPin, HIGH);

        nextPhaseV = redP;

        blinkingGreenPhase();

        lightsTimer = millis() + 300UL;  //3 Sec
        break;


      case redP:

        digitalWrite(digitVeicolYellowPin, LOW);
        digitalWrite(digitVeicolGreenPin, LOW);
        digitalWrite(digitVeicolRedPin, HIGH);

        digitalWrite(digitPedestrianGreenPin, LOW);
        digitalWrite(digitPedestrianRedPin, HIGH);

        //set greeen Veicole
        nextPhaseV = green;
        lightsTimer = millis() + 2000UL;  //2 Sec

        break;
      case green:

        digitalWrite(digitVeicolYellowPin, LOW);
        digitalWrite(digitVeicolGreenPin, HIGH);
        digitalWrite(digitVeicolRedPin, LOW);

        digitalWrite(digitPedestrianGreenPin, LOW);
        digitalWrite(digitPedestrianRedPin, HIGH);

        nextPhaseV = greenYellow;
        lightsTimer = millis() + 45000UL;  //45 Sec
        break;

      case greenYellow:
        digitalWrite(digitVeicolYellowPin, HIGH);
        digitalWrite(digitVeicolGreenPin, HIGH);
        digitalWrite(digitVeicolRedPin, LOW);

        digitalWrite(digitPedestrianGreenPin, LOW);
        digitalWrite(digitPedestrianRedPin, HIGH);

        //set greeen Pedestrian
        nextPhaseV = red;
        lightsTimer = millis() + 7000UL;  //7 Sec

        break;
    }
  }
}




void blinkingGreenPhase() {

  unsigned long int localBlinkingLightsTimer = 0;
  

  blinkingGreenTimer = millis() + maxTimeBlinkingGreen;

  while (millis() < blinkingGreenTimer) {

    if (millis() > localBlinkingLightsTimer) {

      blinking = nextBlinking;
      switch (blinking) {
        case none:

          digitalWrite(digitPedestrianGreenPin, LOW);
          nextBlinking = high;
          localBlinkingLightsTimer = millis() + blinkingTime;
          break;
        case high:

          digitalWrite(digitPedestrianGreenPin, HIGH);
          nextBlinking = none;
          localBlinkingLightsTimer = millis() + blinkingTime;
          break;
      }
    }
  }
  //digitalWrite(digitPedestrianGreenPin, HIGH);
}

void disabledBlinkingPhase() {


  nowTime = millis();


  if (nowTime >= lightsTimer) {
    blinking = nextBlinking;
    switch (blinking) {
      case none:

        digitalWrite(digitVeicolYellowPin, LOW);
        nextBlinking = high;
        lightsTimer = nowTime + blinkingTime;
        break;
      case high:

        digitalWrite(digitVeicolYellowPin, HIGH);
        nextBlinking = none;
        lightsTimer = nowTime + blinkingTime;
        break;
    }
  }
}
